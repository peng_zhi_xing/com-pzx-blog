package com.pzx.blog.mapper;

import com.pzx.blog.entity.Notification;
import com.pzx.blog.entity.NotificationExample;
import java.util.List;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

/**
 * @Description:
 * @Author: PzxF
 **/
@Mapper
@Repository
public interface NotificationMapper {
    long countByExample(NotificationExample example);

    int deleteByExample(NotificationExample example);

    int deleteByPrimaryKey(Integer id);

    int insertSelective(Notification record);

    List<Notification> selectByExample(NotificationExample example);

    Notification selectByPrimaryKey(Integer id);

    int updateByExampleSelective(@Param("record") Notification record, @Param("example") NotificationExample example);

    int updateByExample(@Param("record") Notification record, @Param("example") NotificationExample example);

    int updateByPrimaryKeySelective(Notification record);

    int updateByPrimaryKey(Notification record);


    //以下为自定义方法

    void insert(Notification notification);

    int getunreadcount(Integer id);

    int count(int id);

    List<Notification> list(@Param("id") int id, @Param("offset") int offset, @Param("size") int size);

    void updatestatus(int id);

    int gettypebyid(int id);

    int getouteridbyid(int id);


}