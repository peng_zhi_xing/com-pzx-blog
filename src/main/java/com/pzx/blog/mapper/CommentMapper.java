package com.pzx.blog.mapper;

import com.pzx.blog.entity.Comment;
import com.pzx.blog.entity.CommentExample;
import java.util.List;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

/**
 * @Description:
 * @Author: PzxF
 **/
@Mapper
@Repository
public interface CommentMapper {
    long countByExample(CommentExample example);

    int deleteByExample(CommentExample example);

    int deleteByPrimaryKey(Integer id);

    int insertSelective(Comment record);

    List<Comment> selectByExample(CommentExample example);

    Comment selectByPrimaryKey(Integer id);

    int updateByExampleSelective(@Param("record") Comment record, @Param("example") CommentExample example);

    int updateByExample(@Param("record") Comment record, @Param("example") CommentExample example);

    int updateByPrimaryKeySelective(Comment record);

    int updateByPrimaryKey(Comment record);


    //以下为自定义方法
    List<Comment> getByid(int id);

    void insert(Comment comment);

    List<Comment> getCommentByid(@Param("id") int id, @Param("type") int type);

    void updatecommentcount(int parentId);

    Comment getparentbyid(int parentId);

    String getcontentbyid(int outerId);

    int getparentidbyid(int id);

}