package com.pzx.blog.mapper;

import com.pzx.blog.entity.User;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

/**
 * @Description:
 * @Author: PzxF
 **/
@Mapper
@Repository
public interface UserMapper {

    void insert(User user);

    User select(User user);

    User findBytoken(String token);

    User findById(int createid);

    void updateUserHeadPic(String headpic , Integer id);
}