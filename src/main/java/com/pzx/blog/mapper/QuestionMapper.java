package com.pzx.blog.mapper;

import com.pzx.blog.entity.Question;
import com.pzx.blog.entity.QuestionExample;
import java.util.List;

import org.apache.ibatis.annotations.*;
import org.springframework.stereotype.Repository;

/**
 * @Description:
 * @Author: PzxF
 **/
@Repository
@Mapper
public interface QuestionMapper {
    long countByExample(QuestionExample example);

    int deleteByExample(QuestionExample example);

    int deleteByPrimaryKey(Integer id);

    int insert(Question record);

    int insertSelective(Question record);

    List<Question> selectByExampleWithBLOBs(QuestionExample example);

    List<Question> selectByExample(QuestionExample example);

    Question selectByPrimaryKey(Integer id);

    int updateByExampleSelective(@Param("record") Question record, @Param("example") QuestionExample example);

    int updateByExampleWithBLOBs(@Param("record") Question record, @Param("example") QuestionExample example);

    int updateByExample(@Param("record") Question record, @Param("example") QuestionExample example);

    int updateByPrimaryKeySelective(Question record);

    int updateByPrimaryKeyWithBLOBs(Question record);

    int updateByPrimaryKey(Question record);


    //以下为自定义的方法

    void createquestion(Question question);

    List<Question> list(@Param("offset") int offset, @Param("size") int size);

    int count();

    List<Question> listbyid(@Param("userid") int userid, @Param("offset") int offset, @Param("size") int size);

    int countbyid(int userid);

    Question getbyId(int id);

    void updatequestion(Question question);

    void updateView(int id);

    void updatecomment(int parentId);

    List<Question> getbytag(@Param("id") int id, @Param("result") String result);

    String gettitlebyid(int outerId);

    List<Question> getTopTen();

    List<Question> getQuestions(String title);

    int countQuestion(String title);
}