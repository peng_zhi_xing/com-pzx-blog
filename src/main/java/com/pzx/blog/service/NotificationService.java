package com.pzx.blog.service;

import com.pzx.blog.dto.PageDto;

public interface NotificationService {

    public PageDto list(int id, int page, int size);
}
