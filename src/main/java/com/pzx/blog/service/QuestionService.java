package com.pzx.blog.service;

import com.pzx.blog.dto.PageDto;
import com.pzx.blog.dto.Questiondto;
import com.pzx.blog.entity.Question;

import java.util.List;

public interface QuestionService {
    public PageDto list(int page, int size);

    public PageDto list(int userid, int page, int size);

    public PageDto list(String title, int page, int size);

    public Questiondto getbyid(int id);

    public void increaseview(int id);

    public List<Question> getbytag(int id, String result);

    //public List<Question> getTopTen();



    public List<Question> searchQuestions(String title);




    public  void viewRedis(int id);

    public List<Question> getList();

}
