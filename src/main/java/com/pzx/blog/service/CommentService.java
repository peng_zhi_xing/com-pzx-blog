package com.pzx.blog.service;

import com.pzx.blog.dto.CommentDto;

import java.util.List;

public interface CommentService {

    /**
     * @Description:通过文章id找到所有回复
     * @Author: PzxF
     * @Date: 2021-07-07 09:15:20
     * @param id:
     * @return: java.util.List<com.pzx.blog.dto.CommentDto>
     **/
    public List<CommentDto> getByid(int id);


}
