package com.pzx.blog.service.Impl;

import com.alibaba.druid.support.json.JSONUtils;
import com.pzx.blog.dto.PageDto;
import com.pzx.blog.dto.Questiondto;
import com.pzx.blog.entity.Question;
import com.pzx.blog.entity.User;
import com.pzx.blog.mapper.QuestionMapper;
import com.pzx.blog.mapper.UserMapper;
import com.pzx.blog.service.QuestionService;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;


import java.util.ArrayList;
import java.util.List;
import java.util.Set;

/**
 * @Description:
 * @Author: PzxF
 **/
@Service
public class QuestionServiceImpl implements QuestionService {
    private static final JSONUtils JSON = new JSONUtils();
    @Autowired
    private QuestionMapper questionMapper;
    @Autowired
    private UserMapper userMapper;

    @Override
    public PageDto list(int page, int size) {
        PageDto pageDto = new PageDto();
        int totalcount = questionMapper.count();
        pageDto.setPagination(totalcount,page,size);
        //size*{page-1}
        int offset = size * (page - 1);
        //每页只展示5条
        List<Question> questions = questionMapper.list(offset, size);
        List<Questiondto> questiondtoList = new ArrayList<>();

        for (Question question : questions) {
            User user = userMapper.findById(question.getCreateid());
            Questiondto questiondto = new Questiondto();
            //把第一个对象的所有属性拷贝到第二个对象中
            BeanUtils.copyProperties(question, questiondto);
            questiondto.setUser(user);
            questiondtoList.add(questiondto);
        }
        pageDto.setData(questiondtoList);
        return pageDto;
    }

    @Override
    public PageDto list(int userid, int page, int size) {
        PageDto pageDto = new PageDto();
        int totalcount = questionMapper.countbyid(userid);
        pageDto.setPagination(totalcount,page,size);
        //size*{page-1}
        int offset = size * (page - 1);
        //每页只展示5条
        List<Question> questions = questionMapper.listbyid(userid,offset, size);
        List<Questiondto> questiondtoList = new ArrayList<>();

        for (Question question : questions) {
            User user = userMapper.findById(question.getCreateid());
            Questiondto questiondto = new Questiondto();
            //把第一个对象的所有属性拷贝到第二个对象中
            BeanUtils.copyProperties(question, questiondto);
            questiondto.setUser(user);
            questiondtoList.add(questiondto);
        }
        pageDto.setData(questiondtoList);
        return pageDto;
    }


    @Override
    public PageDto list(String title, int page, int size) {
        PageDto pageDto = new PageDto();
        int totalcount = questionMapper.countQuestion(title);
        pageDto.setPagination(totalcount,page,size);
        //size*{page-1}
        int offset = size * (page - 1);
        //每页只展示5条
        List<Question> questions = questionMapper.getQuestions(title);
        List<Questiondto> questiondtoList = new ArrayList<>();

        for (Question question : questions) {
            User user = userMapper.findById(question.getCreateid());
            Questiondto questiondto = new Questiondto();
            //把第一个对象的所有属性拷贝到第二个对象中
            BeanUtils.copyProperties(question, questiondto);
            questiondto.setUser(user);
            questiondtoList.add(questiondto);
        }
        pageDto.setData(questiondtoList);
        return pageDto;
    }


    @Override
    public Questiondto getbyid(int id) {
        Questiondto questiondto=new Questiondto();
        Question question=questionMapper.getbyId(id);
        //把第一个对象的所有属性拷贝到第二个对象中
        BeanUtils.copyProperties(question, questiondto);
        User user = userMapper.findById(question.getCreateid());
        questiondto.setUser(user);
        return questiondto;
    }

    @Override
    public void increaseview(int id) {
        questionMapper.updateView(id);
    }

    @Override
    public List<Question> getbytag(int id, String result) {
        return questionMapper.getbytag(id,result);
    }

   /*@Override
    public List<Question> getTopTen() {
        return questionMapper.getTopTen();
    }*/





    @Override
    public List<Question> searchQuestions(String title){

        return questionMapper.getQuestions(title);
    }













    @Autowired
    private RedisTemplate<String, Object> redisTemplate;
    public static final String VIEW_RANK = "view_rank";
    @Override
    public void viewRedis(int id){
        redisTemplate.opsForZSet().incrementScore(VIEW_RANK, String.valueOf(id),1);
    }


    /**
     * 获取排行列表
     */
    @Override
    public List<Question> getList(){
        Set<Object> range = redisTemplate.opsForZSet().reverseRange(VIEW_RANK, 0, 10);
        List<Question> list = new ArrayList<>();
        for (Object id : range) {
            list.add(questionMapper.getbyId(Integer.parseInt(id.toString())));
        }
        return list;
    }


}
