package com.pzx.blog.service.Impl;

import com.pzx.blog.mapper.UserMapper;
import com.pzx.blog.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * @program: A_MyBlog
 * @description:
 * @author: PzxF
 * @create: 2021-07-10 14:54
 * @Version 1.0
 **/
public class UserServiceImpl implements UserService {
    
    @Autowired
    private UserMapper userMapper;

    @Override
    public void updateHeadPic(String headpic, Integer id) {
        userMapper.updateUserHeadPic(headpic,id);
    }



}
