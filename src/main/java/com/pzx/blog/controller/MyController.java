package com.pzx.blog.controller;

import com.pzx.blog.entity.User;
import com.pzx.blog.mapper.NotificationMapper;
import com.pzx.blog.mapper.UserMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

/**
 * @program: A_MyBlog
 * @description:
 * @author: PzxF
 * @create: 2021-07-09 16:04
 * @Version 1.0
 **/

@Controller
public class MyController {
    @Autowired
    private UserMapper userMapper;

    @Autowired
    private NotificationMapper notificationMapper;
    @Autowired
    User user;

    @RequestMapping("/my")
    public String my(HttpServletRequest request, Model model,
                     HttpServletResponse response){
        //查找cookies，观察是否有token存在
        //进行登录判断
        Cookie[] cookies = request.getCookies();
        if (cookies == null) {
            PrintWriter out = null;
            try {
                response.setContentType("application/octet-stream;charset=UTF-8");
                out = response.getWriter();
            } catch (IOException e) {
                e.printStackTrace();
            }
            out.println("<script language=\"javaScript\">");
            out.println("alert('尚未登陆,请先进行登录');");
            out.println("</script>");
            return "login";
        }

        for (Cookie cookie : cookies) {
            if (cookie.getName().equals("token")) {
                String token = cookie.getValue();
                user = userMapper.findBytoken(token);
                if (user != null) {
                    request.getSession().setAttribute("user", user);
                    //获取未读的消息数量
                    int unreadnum=notificationMapper.getunreadcount(user.getId());
                    request.getSession().setAttribute("unreadnum",unreadnum);
                }
                break;
            }
        }




        return "my";
    }



/*
    @RequestMapping("/my")
    public String index(HttpServletRequest request, Model model,
                        HttpServletResponse response) {

        //查找cookies，观察是否有token存在
        //进行登录判断
        Cookie[] cookies = request.getCookies();
        if (cookies == null) {
            PrintWriter out = null;
            try {
                response.setContentType("application/octet-stream;charset=UTF-8");
                out = response.getWriter();
            } catch (IOException e) {
                e.printStackTrace();
            }
            out.println("<script language=\"javaScript\">");
            out.println("alert('尚未登陆,请先进行登录');");
            out.println("</script>");
            return "login";
        }

        for (Cookie cookie : cookies) {
            if (cookie.getName().equals("token")) {
                String token = cookie.getValue();
                user = userMapper.findBytoken(token);
                if (user != null) {
                    request.getSession().setAttribute("user", user);
                    //获取未读的消息数量
                    int unreadnum=notificationMapper.getunreadcount(user.getId());
                    request.getSession().setAttribute("unreadnum",unreadnum);
                }
                break;
            }
        }





        return "mysetting";


    }*/

}
