package com.pzx.blog.controller;

import com.pzx.blog.dto.PageDto;
import com.pzx.blog.entity.Question;
import com.pzx.blog.entity.User;
import com.pzx.blog.mapper.NotificationMapper;
import com.pzx.blog.mapper.UserMapper;
import com.pzx.blog.service.Impl.QuestionServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

/**
 * @Description:首页
 * @Author: PzxF
 **/

@Controller
public class IndexController {

    @Autowired
    private UserMapper userMapper;

    @Autowired
    private QuestionServiceImpl questionService;
    @Autowired
    private NotificationMapper notificationMapper;
    @Autowired
    User user;


    @GetMapping("/index")
    public String index(HttpServletRequest request, Model model,
                        HttpServletResponse response,
                        @RequestParam(name = "page", defaultValue = "1") int page,
                        @RequestParam(name = "size", defaultValue = "10") int size) {

        //查找cookies，观察是否有token存在
        //进行登录判断
        Cookie[] cookies = request.getCookies();
        if (cookies == null) {
            PrintWriter out = null;
            try {
                response.setContentType("application/octet-stream;charset=UTF-8");
                out = response.getWriter();
            } catch (IOException e) {
                e.printStackTrace();
            }
            out.println("<script language=\"javaScript\">");
            out.println("alert('尚未登陆,请先进行登录');");
            out.println("</script>");
            return "login";
        }

        for (Cookie cookie : cookies) {
            if (cookie.getName().equals("token")) {
                String token = cookie.getValue();
                user = userMapper.findBytoken(token);
                if (user != null) {
                    request.getSession().setAttribute("user", user);
                    //获取未读的消息数量
                    int unreadnum=notificationMapper.getunreadcount(user.getId());
                    request.getSession().setAttribute("unreadnum",unreadnum);
                }
                break;
            }
        }

        PageDto pagination = questionService.list(page, size);
        model.addAttribute("pagination", pagination);




        //获取阅读量最高的十篇问题
        // 尝试使用redis  √
        List<Question> list = questionService.getList();
        model.addAttribute("topquestion",list);


        //直接从mysql数据库中获取阅读量最高的十篇问题
        /*List<Question> questions= questionService.getTopTen();
        model.addAttribute("topquestion",questions);*/


        return "index";


    }

}
