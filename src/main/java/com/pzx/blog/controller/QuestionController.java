package com.pzx.blog.controller;

import com.pzx.blog.dto.PageDto;
import com.pzx.blog.dto.Questiondto;
import com.pzx.blog.entity.Question;
import com.pzx.blog.entity.User;
import com.pzx.blog.mapper.NotificationMapper;
import com.pzx.blog.mapper.UserMapper;
import com.pzx.blog.service.Impl.CommentServiceImpl;
import com.pzx.blog.service.Impl.QuestionServiceImpl;
import com.pzx.blog.dto.CommentDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
 * @Description:问题详情
 * @Author: PzxF
 **/
@Controller
public class QuestionController {
    @Autowired
    private QuestionServiceImpl questionService;
    @Autowired
    private UserMapper userMapper;
    @Autowired
    private CommentServiceImpl commentService;
    @Autowired
    private NotificationMapper notificationMapper;

    @GetMapping("/question/{id}")
    public String question(@PathVariable(name = "id")int id,
                           Model model,
                           HttpServletRequest request){



        //查找cookies，观察是否有token存在
        Cookie[] cookies = request.getCookies();
        if (cookies == null) {
            return "login";
        }
        User user = null;
        for (Cookie cookie : cookies) {
            if (cookie.getName().equals("token")) {
                String token = cookie.getValue();
                user = userMapper.findBytoken(token);
                if (user != null) {
                    request.getSession().setAttribute("user", user);
                    //获取未读的消息数量
                    int unreadnum=notificationMapper.getunreadcount(user.getId());
                    request.getSession().setAttribute("unreadnum",unreadnum);
                }
                break;
            }
        }






        /**
         * @Description:只是用了Redis做浏览量排行,还没有完全完善
         **/
        Questiondto questiondto=questionService.getbyid(id);
        //Redis
        questionService.viewRedis(id);




        //增加数据库中阅读数
        questionService.increaseview(id);
        model.addAttribute("questionDto",questiondto);
        //展示回复数据
        List<CommentDto> comments=commentService.getByid(id);
        model.addAttribute("comments",comments);
        //相关问题
        String[] tags=questiondto.getTag().split(",");
        StringBuilder msg=new StringBuilder();
        for (String tag:tags){
            msg.append(tag);
            msg.append("|");
        }
        String result=msg.substring(0,msg.length()-1);
        List<Question> relativequestion =questionService.getbytag(id,result);
        model.addAttribute("relativequestion",relativequestion);

        return "question";
    }



    @RequestMapping("/search")
    public String search(HttpServletRequest request,Model model,
                         @RequestParam(name = "page", defaultValue = "1") int page,
                         @RequestParam(name = "size", defaultValue = "10") int size){

        String questiontitle = request.getParameter("questiontitle");
        PageDto pagination = questionService.list(questiontitle,page, size);
        model.addAttribute("pagination", pagination);
        return "index";
    }



}
