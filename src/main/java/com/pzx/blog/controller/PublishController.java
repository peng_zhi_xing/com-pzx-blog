package com.pzx.blog.controller;

import com.pzx.blog.Cache.TagCache;
import com.pzx.blog.dto.TagDto;
import com.pzx.blog.entity.Question;
import com.pzx.blog.entity.User;
import com.pzx.blog.mapper.QuestionMapper;
import com.pzx.blog.mapper.UserMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;


/**
 * @Description:问题发布
 * @Author: PzxF
 **/
@Controller
public class PublishController {
    @Autowired
    private UserMapper userMapper;
    @Autowired
    private QuestionMapper questionMapper;

    @GetMapping("/publish")
    public String publish(Model model,
                          HttpServletRequest request,
                          HttpServletResponse response) {

        //进行登录判断
        Cookie[] cookies = request.getCookies();
        if (cookies == null) {
            PrintWriter out = null;
            try {
                response.setContentType("application/octet-stream;charset=UTF-8");
                out = response.getWriter();
            } catch (IOException e) {
                e.printStackTrace();
            }
            out.println("<script language=\"javaScript\">");
            out.println("alert('尚未登陆,请先进行登录');");
            out.println("</script>");
            return "login";
        }


        //IsLoginUtils.isLogin(request,response);



        //标签组
        TagCache tagCache=new TagCache();
        List<TagDto> tags = tagCache.getTags();
        model.addAttribute("tags",tags);
        return "publish";
    }

    //发布问题
    @PostMapping("/publish")
    public String publishquestion(
            @RequestParam("title") String title,
            @RequestParam("description") String description,
            @RequestParam("tag") String tag,
            @RequestParam(value = "id",defaultValue = "-1")int id,
            HttpServletRequest request,

            //
            HttpServletResponse response,


            Model model
    ) {
        model.addAttribute("title", title);
        model.addAttribute("description", description);
        model.addAttribute("tag", tag);
        //标签组
        TagCache tagCache=new TagCache();
        List<TagDto> tags = tagCache.getTags();
        model.addAttribute("tags",tags);
        //防止输入的问题为空
        if (title == null || title == "") {
            model.addAttribute("error", "标题不能为空");
            return "publish";
        }
        if (description == null || description == "") {
            model.addAttribute("error", "描述不能为空");
            return "publish";
        }
        if (tag == null || tag == "") {
            model.addAttribute("error", "标签不能为空");
            return "publish";
        }
        //获取当前登陆用户的信息

        User user = null;
        Cookie[] cookies = request.getCookies();

  //


        for (Cookie cookie : cookies) {
            if (cookie.getName().equals("token")) {
                String token = cookie.getValue();
                user = userMapper.findBytoken(token);
            }
        }
        //将问题上传到数据库
        Question question = new Question();
        question.setTitle(title);
        question.setDescription(description);
        question.setTag(tag);
        question.setCreateid(user.getId());
        question.setCreatetime(System.currentTimeMillis());
        if(id==-1){
            questionMapper.createquestion(question);
        }else {
            question.setId(id);
            questionMapper.updatequestion(question);
        }
        return "redirect:/index";
    }

    @GetMapping("/publish/{id}")
    public String edit(@PathVariable(name = "id")int id,
                       Model model){
        Question question=questionMapper.getbyId(id);
        model.addAttribute("title", question.getTitle());
        model.addAttribute("description", question.getDescription());
        model.addAttribute("tag", question.getTag());
        //用来标识问题是修改而不是重新创建
        model.addAttribute("id",question.getId());
        return "publish";
    }
}
