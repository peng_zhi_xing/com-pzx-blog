package com.pzx.blog.controller;

import com.pzx.blog.entity.User;
import com.pzx.blog.mapper.UserMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;

/**
 * @program: A_MyBlog
 * @description:
 * @author: PzxF
 * @create: 2021-07-10 15:00
 * @Version 1.0
 **/
@Controller
public class UserController {
    @Autowired
    private UserMapper userMapper;
    @Autowired
    User user;

    @RequestMapping("/headpic")
    public String myHeadPic(HttpServletRequest request, @RequestParam("headpic") String headpic){
        Cookie[] cookies = request.getCookies();
        for (Cookie cookie : cookies) {
            if (cookie.getName().equals("token")) {
                String token = cookie.getValue();
                user = userMapper.findBytoken(token);
            }
        }
        int id = user.getId();
        userMapper.updateUserHeadPic(headpic,id);
        return "mysetting";
    }
}
